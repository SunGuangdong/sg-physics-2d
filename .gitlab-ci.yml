image: registry.gitlab.com/snopek-games/godot-builder-docker:latest

variables:
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/precompiled-binaries/${CI_COMMIT_TAG}"
  APPLICATION_NAME: "SG Physics 2D"
  APPLICATION_URL: "https://www.snopekgames.com"

stages:
  - build
  - sign
  - upload
  - release

build:
  image: registry.gitlab.com/snopek-games/godot-builder-docker:latest
  stage: build
  tags:
    - multicore
  variables:
    GODOT_DOWNLOAD_URL: "https://github.com/godotengine/godot/releases/download/3.5.2-stable/godot-3.5.2-stable.tar.xz"
  parallel:
    matrix:
      - BUILD_TYPE:
        - linux-editor-64
        - linux-export-template-debug-64
        - linux-export-template-release-64
        - windows-editor-64
        - windows-export-template-debug-64
        - windows-export-template-release-64
        - macosx-editor-universal
        - macosx-export-template-debug-universal
        - macosx-export-template-release-universal
        - html5-export-template-debug
        - html5-export-template-release
  script:
    - podman login "$GODOT_BUILD_REGISTRY" $PODMAN_OPTS --username="$GODOT_BUILD_REGISTRY_USERNAME" --password="$GODOT_BUILD_REGISTRY_PASSWORD"
    - ./scripts/build-godot.sh
  rules:
    - if: '$CI_COMMIT_MESSAGE =~ /\[build godot\]/'
      when: always
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+/'
    - if: '$GODOT_BUILD_REGISTRY && $GODOT_BUILD_REGISTRY_USERNAME && $GODOT_BUILD_REGISTRY_PASSWORD'
      changes:
        - godot/**/*
  artifacts:
    name: sg-physics-2d-$BUILD_TYPE
    paths:
      - build/godot/bin

macosx-signed:
  stage: sign
  tags:
    - osx
    - shell
  dependencies:
    - build
  script:
    - mkdir -p build/macosx-signed
    - (cd build/macosx-signed && unzip -a ../godot/bin/macosx-editor-universal.zip)
    - ./scripts/macosx-notarize.sh "./build/macosx-signed/Editor.app"
    - (cd build/macosx-signed && zip -r macosx-editor-universal.zip Editor.app && rm -rf Editor.app)
  artifacts:
    name: sg-physics-2d-$CI_JOB_NAME
    paths:
      - build/macosx-signed
  rules:
    - if: '$CI_COMMIT_MESSAGE =~ /\[build godot\]/'
      when: manual
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+/'
    - if: '$GODOT_BUILD_REGISTRY && $GODOT_BUILD_REGISTRY_USERNAME && $GODOT_BUILD_REGISTRY_PASSWORD'
      when: manual
      changes:
        - godot/**/*

windows-signed:
  stage: sign
  image: ubuntu:20.04
  dependencies:
    - build
  before_script:
    - apt-get update
    - apt-get install -y --no-install-recommends osslsigncode
  script:
    - mkdir -p build/windows-signed
    - cp ./build/godot/bin/windows-editor-64.exe ./build/windows-signed/
    - ./scripts/windows-sign.sh ./build/windows-signed/windows-editor-64.exe "$APPLICATION_NAME" "$APPLICATION_URL"
  artifacts:
    name: sg-physics-2d-$CI_JOB_NAME
    paths:
      - build/windows-signed
  rules:
    - if: '$CI_COMMIT_MESSAGE =~ /\[build godot\]/'
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+/'
    - if: '$GODOT_BUILD_REGISTRY && $GODOT_BUILD_REGISTRY_USERNAME && $GODOT_BUILD_REGISTRY_PASSWORD'
      changes:
        - godot/**/*

upload:
  stage: upload
  image: ubuntu:20.04
  variables:
    PLATFORMS: "linux windows macosx html5"
  before_script:
    - apt-get update
    - apt-get install -y --no-install-recommends curl zip ca-certificates
  script:
    - cp build/macosx-signed/macosx-editor-universal.zip build/godot/bin/
    - cp build/windows-signed/windows-editor-64.exe build/godot/bin/
    - cd build/godot/bin
    - |
      for platform in $PLATFORMS; do fn="sg-physics-2d-$platform-$CI_COMMIT_TAG.zip"; zip $fn $platform-* && curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file $fn ${PACKAGE_REGISTRY_URL}/$fn && rm $fn; done
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+/'
 
release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  dependencies: []
  script:
    - |
      release-cli create --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG \
        --assets-link "{\"name\":\"linux-${CI_COMMIT_TAG}\",\"url\":\"${PACKAGE_REGISTRY_URL}/sg-physics-2d-linux-${CI_COMMIT_TAG}.zip\"}" \
        --assets-link "{\"name\":\"windows-${CI_COMMIT_TAG}\",\"url\":\"${PACKAGE_REGISTRY_URL}/sg-physics-2d-windows-${CI_COMMIT_TAG}.zip\"}" \
        --assets-link "{\"name\":\"macosx-${CI_COMMIT_TAG}\",\"url\":\"${PACKAGE_REGISTRY_URL}/sg-physics-2d-macosx-${CI_COMMIT_TAG}.zip\"}" \
        --assets-link "{\"name\":\"html5-${CI_COMMIT_TAG}\",\"url\":\"${PACKAGE_REGISTRY_URL}/sg-physics-2d-html5-${CI_COMMIT_TAG}.zip\"}"
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+/'

